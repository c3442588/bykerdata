﻿using System;
using bykerdata;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTesting
{
    [TestClass]
    public class SerialiserTest
    {
        /// <summary>
        /// Tests the two methods within the serialser.
        /// </summary>
        [TestMethod]
        public void TestSerialser()
        {
            String test = "test";
            String outcome = null;
            Serializer.WriteToBinaryFile("test.bin",test);
            outcome = Serializer.ReadFromBinaryFile<String>("test.bin");
            Assert.IsTrue(outcome == test);
        }
    }
}
