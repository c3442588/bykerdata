﻿using System;
using bykerdata;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTesting
{
    [TestClass]
    public class SessionDataTest
    {
        SessionData Test = new SessionData();

        [TestMethod]
        public void TestMaxColumn()
        {
            int[,] testarray = new int[5, 1];
            testarray[0, 0] = 5;
            testarray[1, 0] = 4;
            testarray[2, 0] = 3;
            testarray[3, 0] = 2;
            testarray[4, 0] = 1;

            Assert.IsTrue(Test.getMaxColumn(testarray, 0) == "5");
        }


        [TestMethod]
        public void TestAvgColumn()
        {
            int[,] testarray = new int[5, 1];
            testarray[0, 0] = 5;
            testarray[1, 0] = 4;
            testarray[2, 0] = 3;
            testarray[3, 0] = 2;
            testarray[4, 0] = 1;

            Assert.IsTrue(Test.getAVGColumn(testarray, 0) == "3");
        }
        


    }
}
