﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace bykerdata
{
    [Serializable]
    public class SessionData
    {
        private String[] parameters, notes, inttimes, intnotes, extradata, lapnames, summary123, summaryth, hrzones, swaptimes;
        public String[] Trip { get; set; }
        public bool[] SMode { get; set; }
        public bool Speedunit { get; set; }
        public int[,] HRdata { get; set; }
        public int[,] Selecteddata { get; set; }
        private String[] data, model;
        public string Name { get; set; }
        public string Note { get; set; }
        public int Sport { get; set; }
        public int MaxHR { get; set; }
        public int FTP { get; set; }



        /// <summary>
        /// Inputs the data.
        /// </summary>
        /// <param name="file">The file.</param>
        public void inputData(String file)
        {
            data = File.ReadAllLines(file);
            sortData();
        }

        /// <summary>
        /// Sorts the data into arrays from the header.
        /// </summary>
        private void sortData()
        {
            List<String> dataList = data.ToList();
            try
            {
                parameters = dataList.GetRange(dataList.IndexOf("[Params]") + 1, dataList.IndexOf("[Note]") - dataList.IndexOf("[Params]") - 2).ToArray();
            }
            catch
            {
                parameters = dataList.GetRange(dataList.IndexOf("[Params]") + 1, dataList.IndexOf("[HRData]") - dataList.IndexOf("[Params]") - 2).ToArray();
            }
            for (int i = 0; i < parameters.Count(); i++)
            {
                parameters[i] = parameters[i].Substring(parameters[i].IndexOf("=") + 1);
            }
            SModeRead();

            try
            {
                notes = dataList.GetRange(dataList.IndexOf("[Note]") + 1, dataList.IndexOf("[IntTimes]") - dataList.IndexOf("[Note]") - 2).ToArray();
                inttimes = dataList.GetRange(dataList.IndexOf("[IntTimes]") + 1, dataList.IndexOf("[IntNotes]") - dataList.IndexOf("[IntTimes]") - 2).ToArray();
                intnotes = dataList.GetRange(dataList.IndexOf("[IntNotes]") + 1, dataList.IndexOf("[ExtraData]") - dataList.IndexOf("[IntNotes]") - 2).ToArray();
                extradata = dataList.GetRange(dataList.IndexOf("[ExtraData]") + 1, dataList.IndexOf("[LapNames]") - dataList.IndexOf("[ExtraData]") - 2).ToArray();
                lapnames = dataList.GetRange(dataList.IndexOf("[LapNames]") + 1, dataList.IndexOf("[Summary-123]") - dataList.IndexOf("[LapNames]") - 2).ToArray();
                summary123 = dataList.GetRange(dataList.IndexOf("[Summary-123]") + 1, dataList.IndexOf("[Summary-TH]") - dataList.IndexOf("[Summary-123]") - 2).ToArray();
                summaryth = dataList.GetRange(dataList.IndexOf("[Summary-TH]") + 1, dataList.IndexOf("[HRZones]") - dataList.IndexOf("[Summary-TH]") - 2).ToArray();
                hrzones = dataList.GetRange(dataList.IndexOf("[HRZones]") + 1, dataList.IndexOf("[SwapTimes]") - dataList.IndexOf("[HRZones]") - 2).ToArray();
                swaptimes = dataList.GetRange(dataList.IndexOf("[SwapTimes]") + 1, dataList.IndexOf("[Trip]") - dataList.IndexOf("[SwapTimes]") - 2).ToArray();
                Trip = dataList.GetRange(dataList.IndexOf("[Trip]") + 1, dataList.IndexOf("[HRData]") - dataList.IndexOf("[Trip]") - 2).ToArray();
            }
            catch
            {

            }

            var hrdata = dataList.GetRange(dataList.IndexOf("[HRData]") + 1, dataList.Count - dataList.IndexOf("[HRData]") - 1).ToArray();
            if (SMode[7] == true)
            {
                Speedunit = true;
            }
            else
            {
                Speedunit = false;
            }
            this.HRdata = new int[hrdata.Count(), 7];
            for (int y = 0; y < hrdata.Count(); y++)
            {
                StringBuilder row = new StringBuilder();

                var split = hrdata[y].Split(null).ToArray();
                for (int x = 0; x < split.Count(); x++)
                {
                    this.HRdata[y, x] = Convert.ToInt32(split[x]);
                }
            }
            splitPILRB();
            MaxHR = Convert.ToInt32(parameters[17]);
            normalisedPower(false);
            Selecteddata = HRdata;
        }

        /// <summary>
        /// Reads the Smode.
        /// </summary>
        public void SModeRead()
        {
            SMode = new bool[9];
            Console.WriteLine(parameters.Count());
            for (int i = 0; i < parameters[2].Length; i++)
            {
                if (parameters[i] == "0")
                {
                    SMode[i] = false;
                }
                else
                {
                    SMode[i] = true;
                }
            }
        }

        /// <summary>
        /// Gets the average speed.
        /// </summary>
        /// <returns>The Average Speed as double
        /// </returns>
        public double getAvgSpeed(int[,] array)
        {
            int t = 0;
            for (int i = 0; i < array.GetLength(0); i++)
            {
                t = t + array[i, 1];
            }
            Double result = Math.Round((t / array.GetLength(0)) / 10.00, 2);
            return result;
        }

        /// <summary>
        /// Gets the maximum speed.
        /// </summary>
        /// <returns></returns>
        public Double getMaxSpeed(int[,] array)
        {
            List<int> column = new List<int>();
            for (int i = 0; i < array.GetLength(0); i++)
            {
                column.Add(array[i, 1]);
            }

            return column.Max() / 10.00;
        }

        /// <summary>
        /// Gets the distance.
        /// </summary>
        /// <returns></returns>
        public double getDistance()
        {
            try
            {
                return Math.Round(Convert.ToInt32(Trip[0]) / 10.00, 2);
            }
            catch
            {
                return 0;
            }
        }


        /// <summary>
        /// Gets the calories.
        /// </summary>
        /// <returns></returns>
        public String getCalories()
        {
            int mv = Convert.ToInt32(parameters[20]);
            int weight = Convert.ToInt32(parameters[21]);
            double kcalhour = weight * (mv / 3.5);
            TimeSpan ts = TimeSpan.Parse(getDuration());
            double calories = ts.TotalHours * kcalhour;
            return Convert.ToString(Math.Floor(calories));
        }



        /// <summary>
        /// Gets the maximum value in the column.
        /// </summary>
        /// <param name="c">The c.</param>
        /// <returns></returns>
        public String getMaxColumn(int[,] array, int c)
        {
            List<int> column = new List<int>();
            for (int i = 0; i < array.GetLength(0); i++)
            {
                column.Add(array[i, c]);
            }
            return Convert.ToString(column.Max());
        }



        /// <summary>
        /// Gets the average column.
        /// </summary>
        /// <param name="c">The column.</param>
        /// <returns></returns>
        public String getAVGColumn(int[,] array, int c)
        {

            int t = 0;
            for (int i = 0; i < array.GetLength(0); i++)
            {
                t = t + array[i, c];
            }
            Console.WriteLine("total" + t);
            Console.WriteLine("lENGTH" + array.GetLength(0));
            return Convert.ToString(t = t / array.GetLength(0));

        }

        /// <summary>
        /// Gets the date.
        /// </summary>
        /// <returns></returns>
        public String getDate()
        {
            return DateTime.ParseExact(parameters[3], "yyyyMMdd", CultureInfo.InvariantCulture).ToShortDateString();
        }

        /// <summary>
        /// Gets the time.
        /// </summary>
        /// <returns></returns>
        public String getTime()
        {
            return DateTime.ParseExact(parameters[4], "HH:mm:ss.f", CultureInfo.InvariantCulture).ToShortTimeString();
        }

        /// <summary>
        /// Gets the duration.
        /// </summary>
        /// <returns></returns>
        public String getDuration()
        {
            return parameters[5];
        }


        /// <summary>
        /// Gets the model.
        /// </summary>
        /// <returns></returns>
        public String getModel()
        {
            int no = Convert.ToInt32(parameters[1]);
            String name = model[no - 1];
            return name;

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SessionData"/> class.
        /// </summary>
        public SessionData()
        {
            model = "Polar Sport Tester / Vantage XL|Polar Vantage NV (VNV)|Polar Accurex Plus|Polar XTrainer Plus||Polar S520|Polar Coach|Polar S210|Polar S410|Polar S510|Polar S610 / S610i|Polar S710 / S710i / S720i|Polar S810 / S810i||Polar E600|||||Polar AXN500|Polar AXN700|Polar S625X / S725X|Polar S725||||||||||Polar CS400|Polar CS600X|Polar CS600|Polar RS400|Polar RS800|Polar RS800X".Split('|').ToArray();
        }

        /// <summary>
        /// Gets the pb.
        /// </summary>
        /// <returns></returns>
        public String getPB(bool select)
        {
            if (select == true)
            {
                int lb = Convert.ToInt32(getAVGColumn(Selecteddata, 5));
                int rb = 100 - lb;
                return Convert.ToString(lb + "/" + rb + "%");
            }
            else
            {
                int lb = Convert.ToInt32(getAVGColumn(HRdata, 5));
                int rb = 100 - lb;
                return Convert.ToString(lb + "/" + rb + "%");
            }

        }

        /// <summary>
        /// Splits the pilrb.
        /// </summary>
        private void splitPILRB()
        {
            for (int i = 0; i < HRdata.GetLength(0) - 1; i++)
            {
                try
                {
                    ushort number = Convert.ToUInt16(HRdata[i, 5]);
                    byte upper = (byte)(number >> 8);
                    byte lower = (byte)(number & 0xff);
                    int lb = Convert.ToInt32(lower);
                    HRdata[i, 5] = lb;
                    int pi = Convert.ToInt32(upper);
                    HRdata[i, 6] = pi;
                }
                catch
                {
                    HRdata[i, 5] = 0;
                    HRdata[i, 6] = 0;
                }

            }
        }

        /// <summary>
        /// Generates the rolling average of the power generated
        /// </summary>
        /// <returns>The rolling average</returns>
        private long rollingAverage(int interval, int[,] data)
        {
            int roll = 0;
            int width = 30;
            List<double> rollingavg = new List<double>();
            for (int i = 0; i < data.GetLength(0); i++)
            {
                try
                {
                    List<double> sectionavg = new List<double>();
                    for (int ii = roll; ii < width + roll; ii = ii + interval)
                    {
                        sectionavg.Add(data[ii, 4]);
                    }
                    rollingavg.Add(Math.Pow(sectionavg.Average(), 4));
                    roll++;
                }
                catch
                {
                    roll++;
                }
            }
            return Convert.ToInt64(rollingavg.Average());
        }

        /// <summary>
        /// Normalises the power.
        /// </summary>
        /// <returns>String representation of the normalised power</returns>
        public String normalisedPower(bool select)
        {
            long ra;
            if (select == true)
            {
                ra = rollingAverage(Convert.ToInt32(parameters[6]), Selecteddata);
            }
            else
            {
                ra = rollingAverage(Convert.ToInt32(parameters[6]), HRdata);
            }
            ra = Convert.ToInt64(Math.Pow(ra, 1.0 / 4));
            return Convert.ToString(Convert.ToInt32(ra));
        }


        /// <summary>
        /// initialises the selected data array at the specified start witht he specified range.
        /// </summary>
        /// <param name="start">The start.</param>
        /// <param name="range">The range.</param>
        public void initselecteddata(int start, int range)
        {
            Selecteddata = new int[range, 6];
            int ii = 0;
            for (int i = start; i < start + range; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    Selecteddata[ii, j] = HRdata[i, j];
                }
                ii++;
            }
        }

    }
}
