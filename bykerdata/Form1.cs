﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using ZedGraph;

namespace bykerdata
{
    /// <summary>
    /// The main form for the application
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class Form1 : Form
    {
        private OpenFileDialog fd;
        private List<SessionData> sessions = new List<SessionData>();
        private Graph graph1, graph2, graph3;
        private int session = 0;
        public Form1()
        {
            InitializeComponent();
            initFD();
        }

        /// <summary>
        /// Initializes the filedialog.
        /// </summary>
        private void initFD()
        {
            fd = new OpenFileDialog();
            fd.InitialDirectory = "c:\\";
            fd.Filter = "HRM Files (*.hrm)|*.hrm";
            fd.FilterIndex = 2;
            fd.RestoreDirectory = true;
            fd.Multiselect = true;
        }

        /// <summary>
        /// Fills the information fields.
        /// </summary>
        private void fillInfo()
        {
            setSpeedUnit();
            //Main Tab
            textBoxDate.Text = sessions[session].getDate();
            labelDate.Text = sessions[session].getDate();
            textBoxTime.Text = sessions[session].getTime();
            textBoxName.Text = sessions[session].Name;
            comboBoxSport.SelectedIndex = sessions[session].Sport;
            textBoxNote.Text = sessions[session].Note;
            labelDuration.Text = sessions[session].getDuration();
            textBoxModel.Text = sessions[session].getModel();
            labelHRAvg.Text = sessions[session].getAVGColumn(sessions[session].HRdata, 0) + " BPM";
            labelHRMax.Text = sessions[session].getMaxColumn(sessions[session].HRdata, 0) + " BPM";
            labelCalories.Text = sessions[session].getCalories() + " kcal";
            labelAltitude.Text = sessions[session].getAVGColumn(sessions[session].HRdata, 3) + " m";
            textBoxCadenceAvg.Text = sessions[session].getAVGColumn(sessions[session].HRdata, 2) + " Rpm";
            textBoxMaxCadence.Text = sessions[session].getMaxColumn(sessions[session].HRdata, 2) + " Rpm";
            textBoxNormPower.Text = sessions[session].normalisedPower(false) + " W";
            textBoxPwrAvg.Text = sessions[session].getAVGColumn(sessions[session].HRdata, 4) + " W";
            textBoxPwrMax.Text = sessions[session].getMaxColumn(sessions[session].HRdata, 4) + " W";
            textBoxAvgPi.Text = sessions[session].getAVGColumn(sessions[session].HRdata, 6) + " %";
            textBoxMaxPi.Text = sessions[session].getMaxColumn(sessions[session].HRdata, 6) + " %";
            textBoxPB.Text = sessions[session].getPB(false);
            textBoxMaxAltitude.Text = sessions[session].getMaxColumn(sessions[session].HRdata, 3) + " m";
            numericMaxHR.Value = sessions[session].MaxHR;
            textBoxHRPercentage.Text = Convert.ToString(Math.Round((Convert.ToDouble(sessions[session].getAVGColumn(sessions[session].HRdata, 0)) / sessions[session].MaxHR) * 100, 2)) + "%";
            numericFTP.Value = sessions[session].FTP;
            textBoxFTP.Text = Convert.ToString(Math.Round((Convert.ToDouble(sessions[session].getAVGColumn(sessions[session].HRdata, 4)) / sessions[session].FTP) * 100, 2)) + "%";
            textBoxIF.Text = intensityFactor();
            textBoxTSS.Text = trainingStressScore();
            //Comparison Tab
            textBoxCsHRAverage.Text = sessions[session].getAVGColumn(sessions[session].HRdata, 0) + " BPM";
            textBoxCsHRMax.Text = sessions[session].getMaxColumn(sessions[session].HRdata, 0) + " BPM";
            textBoxCsNormalisedPower.Text = sessions[session].normalisedPower(false) + " W";
            textBoxCsPowerAverage.Text = sessions[session].getAVGColumn(sessions[session].HRdata, 4) + " W";
            textBoxCsPowerMax.Text = sessions[session].getMaxColumn(sessions[session].HRdata, 4) + " W";
            textBoxCsCadenceAverage.Text = sessions[session].getAVGColumn(sessions[session].HRdata, 2) + " Rpm";
            textBoxCsCadenceMax.Text = sessions[session].getMaxColumn(sessions[session].HRdata, 2) + " Rpm";

            setGridData();
        }

        /// <summary>
        /// Updates labels and textboxes with information from the selected index
        /// </summary>
        private void selectedInfoUpdate()
        {
            labelHRAvg.Text = sessions[session].getAVGColumn(sessions[session].Selecteddata, 0) + " BPM";
            labelHRMax.Text = sessions[session].getMaxColumn(sessions[session].Selecteddata, 0) + " BPM";
            textBoxCadenceAvg.Text = sessions[session].getAVGColumn(sessions[session].Selecteddata, 2) + " Rpm";
            textBoxMaxCadence.Text = sessions[session].getMaxColumn(sessions[session].Selecteddata, 2) + " Rpm";
            labelAltitude.Text = sessions[session].getAVGColumn(sessions[session].Selecteddata, 3) + " m";
            textBoxMaxAltitude.Text = sessions[session].getMaxColumn(sessions[session].Selecteddata, 3) + " m";
            textBoxPwrAvg.Text = sessions[session].getAVGColumn(sessions[session].Selecteddata, 4) + " W";
            textBoxPwrMax.Text = sessions[session].getMaxColumn(sessions[session].Selecteddata, 4) + " W";
            textBoxAvgPi.Text = sessions[session].getAVGColumn(sessions[session].Selecteddata, 5) + " %";
            textBoxMaxPi.Text = sessions[session].getMaxColumn(sessions[session].Selecteddata, 5) + " %";
            textBoxPB.Text = sessions[session].getPB(true);
            textBoxHRPercentage.Text = Convert.ToString(Math.Round((Convert.ToDouble(sessions[session].getAVGColumn(sessions[session].Selecteddata, 0)) / sessions[session].MaxHR) * 100, 2)) + "%";
            textBoxFTP.Text = Convert.ToString(Math.Round((Convert.ToDouble(sessions[session].getAVGColumn(sessions[session].Selecteddata, 4)) / sessions[session].FTP) * 100, 2)) + "%";
            textBoxNormPower.Text = sessions[session].normalisedPower(true) + " W";
            textBoxIF.Text = intensityFactor();
            textBoxTSS.Text = trainingStressScore();
            setSpeedUnit();
        }

        private void fillComparisonSession()
        {
            graph3 = new Graph(sessions[comboBoxComparison.SelectedIndex].HRdata);
            ComparisonGraphControl2.GraphPane = graph3.Pane;
            ComparisonGraphControl2.IsShowHScrollBar = true;
            ComparisonGraphControl2.IsAutoScrollRange = true;
            ComparisonGraphControl2.IsEnableHZoom = true;
            ComparisonGraphControl2.IsEnableVZoom = false;
            ComparisonGraphControl2.IsEnableHPan = false;
            ComparisonGraphControl2.AxisChange();
            foreach (LineItem li in ComparisonGraphControl2.GraphPane.CurveList)
            {
                li.Line.Width = 2.0F;
            }

            textBoxSsHRAverage.Text = sessions[comboBoxComparison.SelectedIndex].getAVGColumn(sessions[comboBoxComparison.SelectedIndex].HRdata, 0) + " BPM";
            textBoxSsHRMax.Text = sessions[comboBoxComparison.SelectedIndex].getMaxColumn(sessions[comboBoxComparison.SelectedIndex].HRdata, 0) + " BPM";
            textBoxSsNormalisedPower.Text = sessions[comboBoxComparison.SelectedIndex].normalisedPower(false) + " W";
            textBoxSsPowerAverage.Text = sessions[comboBoxComparison.SelectedIndex].getAVGColumn(sessions[comboBoxComparison.SelectedIndex].HRdata, 4) + " W";
            textBoxSsPowerMax.Text = sessions[comboBoxComparison.SelectedIndex].getMaxColumn(sessions[comboBoxComparison.SelectedIndex].HRdata, 4) + " W";
            textBoxSsCadenceAverage.Text = sessions[comboBoxComparison.SelectedIndex].getAVGColumn(sessions[comboBoxComparison.SelectedIndex].HRdata, 2) + " Rpm";
            textBoxSsCadenceMax.Text = sessions[comboBoxComparison.SelectedIndex].getMaxColumn(sessions[comboBoxComparison.SelectedIndex].HRdata, 2) + " Rpm";
            if (sessions[comboBoxComparison.SelectedIndex].Speedunit == true)
            {
                comboBoxunit.SelectedIndex = 0;
                if (sessions[comboBoxComparison.SelectedIndex].SMode[7] == true)
                {
                    textBoxSsSpeedAverage.Text = Convert.ToString(Math.Round(sessions[comboBoxComparison.SelectedIndex].getAvgSpeed(sessions[comboBoxComparison.SelectedIndex].HRdata), 2) + "Mph");
                    textBoxSsSpeedMax.Text = Convert.ToString(Math.Round(sessions[comboBoxComparison.SelectedIndex].getMaxSpeed(sessions[comboBoxComparison.SelectedIndex].HRdata), 2) + "Mph");
                }
                else
                {
                    textBoxSsSpeedAverage.Text = Convert.ToString(Math.Round((sessions[comboBoxComparison.SelectedIndex].getAvgSpeed(sessions[comboBoxComparison.SelectedIndex].HRdata) * 0.621371), 2) + "Mph");
                    textBoxSsSpeedMax.Text = Convert.ToString(Math.Round((sessions[comboBoxComparison.SelectedIndex].getMaxSpeed(sessions[comboBoxComparison.SelectedIndex].HRdata) * 0.621371), 2) + "Mph");
                }
            }
            else
            {
                comboBoxunit.SelectedIndex = 1;
                if (sessions[comboBoxComparison.SelectedIndex].SMode[7] == false)
                {
                    textBoxSsSpeedAverage.Text = Convert.ToString(Math.Round(sessions[comboBoxComparison.SelectedIndex].getAvgSpeed(sessions[comboBoxComparison.SelectedIndex].HRdata), 2) + "Kph");
                    textBoxSsSpeedMax.Text = Convert.ToString(Math.Round(sessions[comboBoxComparison.SelectedIndex].getMaxSpeed(sessions[comboBoxComparison.SelectedIndex].HRdata), 2) + "Kph");

                }
                else
                {
                    textBoxSsSpeedAverage.Text = Convert.ToString(Math.Round((sessions[comboBoxComparison.SelectedIndex].getAvgSpeed(sessions[comboBoxComparison.SelectedIndex].HRdata) * 1.60934), 2) + "Kph");
                    textBoxSsSpeedMax.Text = Convert.ToString(Math.Round((sessions[comboBoxComparison.SelectedIndex].getMaxSpeed(sessions[comboBoxComparison.SelectedIndex].HRdata) * 1.60934), 2) + "Kph");
                }
            }
        }




        /// <summary>
        /// Sets the speed unit.
        /// </summary>
        private void setSpeedUnit()
        {
            if (sessions[session].Speedunit == true)
            {
                comboBoxunit.SelectedIndex = 0;
                if (sessions[session].SMode[7] == true)
                {
                    labelSpeed.Text = Convert.ToString(Math.Round(sessions[session].getAvgSpeed(sessions[session].Selecteddata), 2) + "Mph");
                    textBoxMaxSpeed.Text = Convert.ToString(Math.Round(sessions[session].getMaxSpeed(sessions[session].Selecteddata), 2) + "Mph");
                    labelDistance.Text = Convert.ToString(Math.Round(sessions[session].getDistance(), 2) + "Mi");
                    labelAltitude.Text = sessions[session].getAVGColumn(sessions[session].Selecteddata, 3) + " ft";
                    textBoxMaxAltitude.Text = sessions[session].getMaxColumn(sessions[session].Selecteddata, 3) + " ft";
                    //comparison window
                    textBoxCsSpeedAverage.Text = Convert.ToString(Math.Round(sessions[session].getAvgSpeed(sessions[session].HRdata), 2) + "Mph");
                    textBoxCsSpeedMax.Text = Convert.ToString(Math.Round(sessions[session].getMaxSpeed(sessions[session].HRdata), 2) + "Mph");
                }
                else
                {
                    labelSpeed.Text = Convert.ToString(Math.Round((sessions[session].getAvgSpeed(sessions[session].Selecteddata) * 0.621371), 2) + "Mph");
                    textBoxMaxSpeed.Text = Convert.ToString(Math.Round((sessions[session].getMaxSpeed(sessions[session].Selecteddata) * 0.621371), 2) + "Mph");
                    labelDistance.Text = Convert.ToString(Math.Round((sessions[session].getDistance() * 0.621371), 2) + "Mi");
                    labelAltitude.Text = Convert.ToString(Math.Floor(Convert.ToUInt32(sessions[session].getAVGColumn(sessions[session].Selecteddata, 3)) * 3.28084)) + " ft";
                    textBoxMaxAltitude.Text = Convert.ToString(Math.Floor(Convert.ToUInt32(sessions[session].getMaxColumn(sessions[session].Selecteddata, 3)) * 3.28084)) + " ft";
                    //comparison window
                    textBoxCsSpeedAverage.Text = Convert.ToString(Math.Round((sessions[session].getAvgSpeed(sessions[session].HRdata) * 0.621371), 2) + "Mph");
                    textBoxCsSpeedMax.Text = Convert.ToString(Math.Round((sessions[session].getMaxSpeed(sessions[session].HRdata) * 0.621371), 2) + "Mph");
                }
            }
            else
            {
                comboBoxunit.SelectedIndex = 1;
                if (sessions[session].SMode[7] == false)
                {
                    labelSpeed.Text = Convert.ToString(Math.Round(sessions[session].getAvgSpeed(sessions[session].Selecteddata), 2) + "Kph");
                    textBoxMaxSpeed.Text = Convert.ToString(Math.Round(sessions[session].getMaxSpeed(sessions[session].Selecteddata), 2) + "Kph");
                    labelDistance.Text = Convert.ToString(Math.Round(sessions[session].getDistance(), 2) + "Km");
                    labelAltitude.Text = sessions[session].getAVGColumn(sessions[session].Selecteddata, 3) + " m";
                    textBoxMaxAltitude.Text = sessions[session].getMaxColumn(sessions[session].Selecteddata, 3) + " m";
                    //comparison window
                    textBoxCsSpeedAverage.Text = Convert.ToString(Math.Round(sessions[session].getAvgSpeed(sessions[session].HRdata), 2) + "Kph");
                    textBoxCsSpeedMax.Text = Convert.ToString(Math.Round(sessions[session].getMaxSpeed(sessions[session].HRdata), 2) + "Kph");

                }
                else
                {
                    labelSpeed.Text = Convert.ToString(Math.Round((sessions[session].getAvgSpeed(sessions[session].Selecteddata) * 1.60934), 2) + "Kph");
                    textBoxMaxSpeed.Text = Convert.ToString(Math.Round((sessions[session].getMaxSpeed(sessions[session].Selecteddata) * 1.60934), 2) + "Kph");
                    labelDistance.Text = Convert.ToString(Math.Round((sessions[session].getDistance() * 1.60934), 2) + "Km");
                    labelAltitude.Text = Convert.ToString(Math.Floor(Convert.ToUInt32(sessions[session].getAVGColumn(sessions[session].Selecteddata, 3)) * 0.3048)) + " m";
                    textBoxMaxAltitude.Text = Convert.ToString(Math.Floor(Convert.ToUInt32(sessions[session].getMaxColumn(sessions[session].Selecteddata, 3)) * 0.3048)) + " m";
                    //comparison Window
                    textBoxCsSpeedAverage.Text = Convert.ToString(Math.Round((sessions[session].getAvgSpeed(sessions[session].HRdata) * 1.60934), 2) + "Kph");
                    textBoxCsSpeedMax.Text = Convert.ToString(Math.Round((sessions[session].getMaxSpeed(sessions[session].HRdata) * 1.60934), 2) + "Kph");
                }
            }
        }

        /// <summary>
        /// Sets the selected speed unit.
        /// </summary>
        private void setSelectedSpeedUnit()
        {
            if (sessions[session].Speedunit == true)
            {
                comboBoxunit.SelectedIndex = 0;
                if (sessions[session].SMode[7] == true)
                {
                    labelSpeed.Text = Convert.ToString(Math.Round(sessions[session].getAvgSpeed(sessions[session].HRdata), 2) + "Mph");
                    textBoxMaxSpeed.Text = Convert.ToString(Math.Round(sessions[session].getMaxSpeed(sessions[session].HRdata), 2) + "Mph");
                    labelDistance.Text = Convert.ToString(Math.Round(sessions[session].getDistance(), 2) + "Mi");
                    labelAltitude.Text = sessions[session].getAVGColumn(sessions[session].HRdata, 3) + " ft";
                    textBoxMaxAltitude.Text = sessions[session].getMaxColumn(sessions[session].HRdata, 3) + " ft";
                }
                else
                {
                    labelSpeed.Text = Convert.ToString(Math.Round((sessions[session].getAvgSpeed(sessions[session].HRdata) * 0.621371), 2) + "Mph");
                    textBoxMaxSpeed.Text = Convert.ToString(Math.Round((sessions[session].getMaxSpeed(sessions[session].HRdata) * 0.621371), 2) + "Mph");
                    labelDistance.Text = Convert.ToString(Math.Round((sessions[session].getDistance() * 0.621371), 2) + "Mi");
                    labelAltitude.Text = Convert.ToString(Math.Floor(Convert.ToUInt32(sessions[session].getAVGColumn(sessions[session].HRdata, 3)) * 3.28084)) + " ft";
                    textBoxMaxAltitude.Text = Convert.ToString(Math.Floor(Convert.ToUInt32(sessions[session].getMaxColumn(sessions[session].HRdata, 3)) * 3.28084)) + " ft";
                }
            }
            else
            {
                comboBoxunit.SelectedIndex = 1;
                if (sessions[session].SMode[7] == false)
                {
                    labelSpeed.Text = Convert.ToString(Math.Round(sessions[session].getAvgSpeed(sessions[session].HRdata), 2) + "Kph");
                    textBoxMaxSpeed.Text = Convert.ToString(Math.Round(sessions[session].getMaxSpeed(sessions[session].HRdata), 2) + "Kph");
                    labelDistance.Text = Convert.ToString(Math.Round(sessions[session].getDistance(), 2) + "Km");
                    labelAltitude.Text = sessions[session].getAVGColumn(sessions[session].HRdata, 3) + " m";
                    textBoxMaxAltitude.Text = sessions[session].getMaxColumn(sessions[session].HRdata, 3) + " m";
                }
                else
                {
                    labelSpeed.Text = Convert.ToString(Math.Round((sessions[session].getAvgSpeed(sessions[session].HRdata) * 1.60934), 2) + "Kph");
                    textBoxMaxSpeed.Text = Convert.ToString(Math.Round((sessions[session].getMaxSpeed(sessions[session].HRdata) * 1.60934), 2) + "Kph");
                    labelDistance.Text = Convert.ToString(Math.Round((sessions[session].getDistance() * 1.60934), 2) + "Km");
                    labelAltitude.Text = Convert.ToString(Math.Floor(Convert.ToUInt32(sessions[session].getAVGColumn(sessions[session].HRdata, 3)) * 0.3048)) + " m";
                    textBoxMaxAltitude.Text = Convert.ToString(Math.Floor(Convert.ToUInt32(sessions[session].getMaxColumn(sessions[session].HRdata, 3)) * 0.3048)) + " m";
                }
            }
        }

        /// <summary>
        /// Draws the graph.
        /// </summary>
        private void drawGraph()
        {
            graph1 = new Graph(sessions[session].HRdata);
            zedGraphControl.GraphPane = graph1.Pane;
            zedGraphControl.IsShowHScrollBar = true;
            zedGraphControl.IsAutoScrollRange = true;
            zedGraphControl.IsEnableHZoom = true;
            zedGraphControl.IsEnableVZoom = false;
            zedGraphControl.IsEnableHPan = false;
            zedGraphControl.AxisChange();
            foreach (LineItem li in zedGraphControl.GraphPane.CurveList)
            {
                li.Line.Width = 2.0F;
            }
        }

        private void drawComparisonGraph1()
        {
            graph2 = new Graph(sessions[session].HRdata);
            ComparisonGraphControl1.GraphPane = graph2.Pane;
            ComparisonGraphControl1.IsShowHScrollBar = true;
            ComparisonGraphControl1.IsAutoScrollRange = true;
            ComparisonGraphControl1.IsEnableHZoom = true;
            ComparisonGraphControl1.IsEnableVZoom = false;
            ComparisonGraphControl1.IsEnableHPan = false;
            ComparisonGraphControl1.AxisChange();
            foreach (LineItem li in ComparisonGraphControl1.GraphPane.CurveList)
            {
                li.Line.Width = 2.0F;
            }
        }



        /// <summary>
        /// Handles the Click event of the importToolStripMenuItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void importToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (fd.ShowDialog() == DialogResult.OK)
            {
                foreach (String file in fd.FileNames)
                {
                    SessionData import = new SessionData();
                    import.inputData(file);
                    sessions.Add(import);
                }
                session = sessions.Count - 1;
                fillInfo();
                tabControl.SelectedIndex = 0;
                drawGraph();
                drawComparisonGraph1();
            }

        }


        /// <summary>
        /// Handles the Load event of the Form1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                sessions = Serializer.ReadFromBinaryFile<List<SessionData>>("sessions.bin");
                fillInfo();
                drawGraph();
                drawComparisonGraph1();
                foreach (SessionData session in sessions)
                {
                    comboBoxSession.Items.Add(session.Name);
                    comboBoxComparison.Items.Add(session.Name);
                }
                comboBoxSession.SelectedIndex = session;
                trainingStressScore();
            }
            catch
            {

            }
        }

        /// <summary>
        /// Handles the FormClosing event of the Form1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="FormClosingEventArgs"/> instance containing the event data.</param>
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Serializer.WriteToBinaryFile("sessions.bin", sessions);
        }

        /// <summary>
        /// Handles the TextChanged event of the textBoxName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void textBoxName_TextChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the comboBoxSport control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void comboBoxSport_SelectedIndexChanged(object sender, EventArgs e)
        {
            sessions[session].Sport = comboBoxSport.SelectedIndex;
        }




        /// <summary>
        /// Sets the grid data.
        /// </summary>
        private void setGridData()
        {
            int height = sessions[session].HRdata.GetLength(0);
            int width = sessions[session].HRdata.GetLength(1);
            this.dataGridView1.ColumnCount = width;
            dataGridView1.Columns[0].HeaderText = "HR(BPM)";
            dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns[1].HeaderText = "Speed(Kph)";
            dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns[2].HeaderText = "Cadence(Rpm)";
            dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns[3].HeaderText = "Altitude(m)";
            dataGridView1.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns[4].HeaderText = "Power(Watts)";
            dataGridView1.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns[5].HeaderText = "Power Balance(%)";
            dataGridView1.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns[6].HeaderText = "Power Index(%)";
            dataGridView1.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            for (int r = 0; r < height; r++)
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(this.dataGridView1);
                for (int c = 0; c < width; c++)
                {
                    row.Cells[c].Value = sessions[session].HRdata[r, c];
                }

                this.dataGridView1.Rows.Add(row);
            }
        }

        /// <summary>
        /// Handles the SelectedValueChanged event of the comboBoxunit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void comboBoxunit_SelectedValueChanged(object sender, EventArgs e)
        {
            if (comboBoxunit.SelectedIndex == 0)
            {
                sessions[session].Speedunit = true;
                setSpeedUnit();
                drawGraph();
                drawComparisonGraph1();
            }
            else if (comboBoxunit.SelectedIndex == 1)
            {
                sessions[session].Speedunit = false;
                setSpeedUnit();
                drawGraph();
                drawComparisonGraph1();
            }
        }


        /// <summary>
        /// Handles the TextChanged event of the textBoxNote control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void textBoxNote_TextChanged(object sender, EventArgs e)
        {
            sessions[session].Note = textBoxNote.Text;
        }

        /// <summary>
        /// Handles the ValueChanged event of the numericMaxHR control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void numericMaxHR_ValueChanged(object sender, EventArgs e)
        {
            sessions[session].MaxHR = Convert.ToInt32(numericMaxHR.Value);
            textBoxHRPercentage.Text = Convert.ToString(Math.Round((Convert.ToDouble(sessions[session].getAVGColumn(sessions[session].HRdata, 0)) / sessions[session].MaxHR) * 100, 2)) + "%";
        }

        /// <summary>
        /// Handles the ValueChanged event of the numericFTP control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void numericFTP_ValueChanged(object sender, EventArgs e)
        {
            sessions[session].FTP = Convert.ToInt32(numericFTP.Value);
            textBoxFTP.Text = Convert.ToString(Math.Round((Convert.ToDouble(sessions[session].normalisedPower(true)) / sessions[session].FTP) * 100, 2)) + "%";
            intensityFactor();
            trainingStressScore();
        }

        /// <summary>
        /// Calculates the intesnity factor
        /// </summary>
        /// <returns></returns>
        private String intensityFactor()
        {
            try
            {
                double ftp = Convert.ToDouble(numericFTP.Value);
                double np = Convert.ToDouble(sessions[session].normalisedPower(true));
                textBoxIF.Text = Convert.ToString(Math.Round(np / ftp, 2));
                return Convert.ToString(Math.Round(np / ftp, 2));
            }
            catch
            {
                return "0";
            }

        }



        /// <summary>
        /// Calculates the training stress Score
        /// </summary>
        /// <returns> The TSS</returns>
        private String trainingStressScore()
        {
            double sec, np, IF, FTP, TSS;
            sec = TimeSpan.Parse(sessions[session].getDuration()).TotalSeconds;
            np = Convert.ToDouble(sessions[session].normalisedPower(true));
            IF = Convert.ToDouble(intensityFactor());
            FTP = Convert.ToDouble(numericFTP.Value);
            TSS = Math.Floor((sec * np * IF) / (FTP * 3600) * 100);
            textBoxTSS.Text = Convert.ToString(TSS);
            return Convert.ToString(TSS);
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the comboBoxSession control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void comboBoxSession_SelectedIndexChanged(object sender, EventArgs e)
        {
            session = comboBoxSession.SelectedIndex;
            fillInfo();
            drawGraph();
            drawComparisonGraph1();
        }

        /// <summary>
        /// Handles the KeyDown event of the textBoxName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
        private void textBoxName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                sessions[session].Name = textBoxName.Text;
                comboBoxSession.SelectedText = textBoxName.Text;
                comboBoxSession.Items.Clear();
                comboBoxComparison.Items.Clear();
                foreach (SessionData session in sessions)
                {
                    comboBoxSession.Items.Add(session.Name);
                    comboBoxComparison.Items.Add(session.Name);
                }
                comboBoxSession.SelectedIndex = session;
            }
        }

        /// <summary>
        /// Zeds the graph control zoom event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="oldState">The old state.</param>
        /// <param name="newState">The new state.</param>
        private void zedGraphControl_ZoomEvent(ZedGraphControl sender, ZoomState oldState, ZoomState newState)
        {
            int min = Convert.ToInt32(Math.Round(sender.GraphPane.XAxis.Scale.Min, 0));
            int max = Convert.ToInt32(Math.Round(sender.GraphPane.XAxis.Scale.Max, 0));
            sessions[session].initselecteddata(min, max - min);
            selectedInfoUpdate();
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the comboBoxComparison control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void comboBoxComparison_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillComparisonSession();
        }

        /// <summary>
        /// Handles the MouseMove event of the ComparisonGraphControl2 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void ComparisonGraphControl2_MouseMove(object sender, MouseEventArgs e)
        {
            mouseComparison(e.Location);
        }

        /// <summary>
        /// Handles the MouseMove event of the ComparisonGraphControl1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void ComparisonGraphControl1_MouseMove(object sender, MouseEventArgs e)
        {
            mouseComparison(e.Location);
        }


        /// <summary>
        /// Displays the data gathered from the mouse position in the corresponding text boxes
        /// </summary>
        /// <param name="coord">The coord.</param>
        private void mouseComparison(PointF coord)
        {
            double x1, x2;
            double y1, y2;
            String[] values1,values2;
            try
            {
                ComparisonGraphControl1.GraphPane.ReverseTransform(coord, out x1, out y1);
                ComparisonGraphControl2.GraphPane.ReverseTransform(coord, out x2, out y2);
                values1 = graph2.getValues(x1);
                values2 = graph3.getValues(x2);
                textBoxHRComp.Text = "HR:" + values1[0] + "/" + values2[0]+" BPM ";
                textBoxSpeedComp.Text = "Speed:" + values1[1] + "/" + values2[1] + " Kmh/Mph";
                textBoxCadenceComp.Text = "Cadence:" + values1[2] + "/" + values2[2] + " RPM";
                textBoxAltitudeComp.Text = "Altitude:" + values1[3] + "/" + values2[3] + " M/Ft";
                textBoxPowerComp.Text = "Power:" + values1[4] + "/" + values2[4] + " W";
            }
            catch
            {

            }


        }
    }
}
