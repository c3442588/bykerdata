﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using ZedGraph;

namespace bykerdata
{
    /// <summary>
    /// A class to handle the operations peformed ont he zedgraph control
    /// </summary>
    class Graph
    {
        private int[,] data;
        public GraphPane Pane { get; set; }
        public PointPairList HRPairList { get; set; }
        public PointPairList SpeedPairList { get; set; }
        public PointPairList CadencePairList { get; set; }
        public PointPairList AltitudePairList { get; set; }
        public PointPairList PowerPairList { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="Graph"/> class.
        /// </summary>
        /// <param name="data">The data.</param>
        public Graph(int[,] data)
        {
            Pane = new GraphPane();
            Pane.Margin.All = 0;
            Pane.Border.IsVisible = false;
            Pane.Chart.Border.IsVisible = false;
            this.data = data;
            plot();            
        }

        /// <summary>
        /// Gets the y value from the point pair list with the corresponding x value
        /// </summary>
        /// <param name="x">The x.</param>
        /// <returns></returns>
        public string[] getValues(double x)
        {
            String[] values = new String[5];
            values[0] = Convert.ToString(Math.Floor(HRPairList.InterpolateX(x)));
            values[1] = Convert.ToString(Math.Floor(SpeedPairList.InterpolateX(x)));
            values[2] = Convert.ToString(Math.Floor(CadencePairList.InterpolateX(x)));
            values[3] = Convert.ToString(Math.Floor(AltitudePairList.InterpolateX(x)));
            values[4] = Convert.ToString(Math.Floor(PowerPairList.InterpolateX(x)));
            return values;
        }


        /// <summary>
        /// Plots this Graph.
        /// </summary>
        public void plot()
        {
            Pane.YAxisList.Clear();
            var y1 = Pane.AddYAxis("HR (Bpm");
            var y2 = Pane.AddYAxis("Speed (Kph)");
            var y3 = Pane.AddYAxis("Cadence (Rpm)");
            var y4 = Pane.AddYAxis("Altitude (m)");
            var y5 = Pane.AddYAxis("Power (w)");
            Pane.XAxis.Scale.Max = data.GetLength(0);



            // Set the Titles      


            Pane.XAxis.Scale.MajorStep = 60;
            Pane.XAxis.Scale.MinorStep = 1;

            HRPairList = new PointPairList();
            SpeedPairList = new PointPairList();
            CadencePairList = new PointPairList();
            AltitudePairList = new PointPairList();
            PowerPairList = new PointPairList();

            for (int i = 0; i < data.GetLength(0); i++)
            {
                HRPairList.Add(i, data[i, 0]);
                SpeedPairList.Add(i, data[i, 1] / 10);
                CadencePairList.Add(i, data[i, 2]);
                AltitudePairList.Add(i, data[i, 3]);
                PowerPairList.Add(i, data[i, 4]);
            }

            LineItem HRCurve = Pane.AddCurve("HR(Bpm)",
                   HRPairList, Color.Red, SymbolType.None);
            HRCurve.YAxisIndex = y1;

            LineItem SpeedCurve = Pane.AddCurve("Speed(Kph)",
                  SpeedPairList, Color.Blue, SymbolType.None);
            SpeedCurve.YAxisIndex = y2;

            LineItem CadenceCurve = Pane.AddCurve("Cadence(Rpm)",
                  CadencePairList, Color.Green, SymbolType.None);
            CadenceCurve.YAxisIndex = y3;

            LineItem AltitudeCurve = Pane.AddCurve("Altitude(m)",
                  AltitudePairList, Color.DeepPink, SymbolType.None);
            AltitudeCurve.YAxisIndex = y4;

            LineItem PowerCurve = Pane.AddCurve("Power(w)",
                  PowerPairList, Color.Orange, SymbolType.None);
            PowerCurve.YAxisIndex = y5;
        }
    }
}

