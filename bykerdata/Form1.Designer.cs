﻿using System.Windows.Forms;

namespace bykerdata
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.zedGraphControl = new ZedGraph.ZedGraphControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.numericFTP = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.textBoxDate = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxSport = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxTime = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.comboBoxunit = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.numericMaxHR = new System.Windows.Forms.NumericUpDown();
            this.textBoxNote = new System.Windows.Forms.TextBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxMaxCadence = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxCadenceAvg = new System.Windows.Forms.TextBox();
            this.textBoxModel = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxMaxSpeed = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxPwrAvg = new System.Windows.Forms.TextBox();
            this.textBoxPwrMax = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxAvgPi = new System.Windows.Forms.TextBox();
            this.textBoxMaxPi = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxPB = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBoxHRPercentage = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBoxFTP = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBoxMaxAltitude = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.textBoxNormPower = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.textBoxIF = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.textBoxTSS = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.textBoxSsCadenceAverage = new System.Windows.Forms.TextBox();
            this.textBoxSsCadenceMax = new System.Windows.Forms.TextBox();
            this.textBoxSsSpeedAverage = new System.Windows.Forms.TextBox();
            this.textBoxSsSpeedMax = new System.Windows.Forms.TextBox();
            this.textBoxSsNormalisedPower = new System.Windows.Forms.TextBox();
            this.textBoxSsPowerAverage = new System.Windows.Forms.TextBox();
            this.textBoxSsPowerMax = new System.Windows.Forms.TextBox();
            this.textBoxSsHRAverage = new System.Windows.Forms.TextBox();
            this.textBoxSsHRMax = new System.Windows.Forms.TextBox();
            this.comboBoxComparison = new System.Windows.Forms.ComboBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.ComparisonGraphControl1 = new ZedGraph.ZedGraphControl();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.ComparisonGraphControl2 = new ZedGraph.ZedGraphControl();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.textBoxCsCadenceAverage = new System.Windows.Forms.TextBox();
            this.textBoxCsCadenceMax = new System.Windows.Forms.TextBox();
            this.textBoxCsSpeedAverage = new System.Windows.Forms.TextBox();
            this.textBoxCsSpeedMax = new System.Windows.Forms.TextBox();
            this.textBoxCsNormalisedPower = new System.Windows.Forms.TextBox();
            this.textBoxCsPowerAverage = new System.Windows.Forms.TextBox();
            this.textBoxCsPowerMax = new System.Windows.Forms.TextBox();
            this.textBoxCsHRAverage = new System.Windows.Forms.TextBox();
            this.textBoxCsHRMax = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.label25 = new System.Windows.Forms.Label();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.textBoxCadenceComp = new System.Windows.Forms.TextBox();
            this.textBoxAltitudeComp = new System.Windows.Forms.TextBox();
            this.textBoxHRComp = new System.Windows.Forms.TextBox();
            this.textBoxPowerComp = new System.Windows.Forms.TextBox();
            this.textBoxSpeedComp = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.labelSpeed = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.labelDistance = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.labelCalories = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.labelAltitude = new System.Windows.Forms.Label();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.labelHRMax = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.labelDuration = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.labelHRAvg = new System.Windows.Forms.Label();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.labelDate = new System.Windows.Forms.Label();
            this.comboBoxSession = new System.Windows.Forms.ComboBox();
            this.menuStrip1.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericFTP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMaxHR)).BeginInit();
            this.groupBox9.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1184, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // importToolStripMenuItem
            // 
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            this.importToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.importToolStripMenuItem.Text = "Import";
            this.importToolStripMenuItem.Click += new System.EventHandler(this.importToolStripMenuItem_Click);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage3);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(3, 83);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1178, 639);
            this.tabControl.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tableLayoutPanel11);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1170, 613);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Summary";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 1;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Controls.Add(this.zedGraphControl, 0, 1);
            this.tableLayoutPanel11.Controls.Add(this.tableLayoutPanel1, 0, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 2;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 72.3229F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 27.6771F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(1164, 607);
            this.tableLayoutPanel11.TabIndex = 1;
            // 
            // zedGraphControl
            // 
            this.zedGraphControl.AutoScroll = true;
            this.zedGraphControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zedGraphControl.IsAntiAlias = true;
            this.zedGraphControl.IsEnableWheelZoom = false;
            this.zedGraphControl.Location = new System.Drawing.Point(0, 438);
            this.zedGraphControl.Margin = new System.Windows.Forms.Padding(0);
            this.zedGraphControl.Name = "zedGraphControl";
            this.zedGraphControl.ScrollGrace = 0D;
            this.zedGraphControl.ScrollMaxX = 0D;
            this.zedGraphControl.ScrollMaxY = 0D;
            this.zedGraphControl.ScrollMaxY2 = 0D;
            this.zedGraphControl.ScrollMinX = 0D;
            this.zedGraphControl.ScrollMinY = 0D;
            this.zedGraphControl.ScrollMinY2 = 0D;
            this.zedGraphControl.Size = new System.Drawing.Size(1164, 169);
            this.zedGraphControl.TabIndex = 0;
            this.zedGraphControl.UseExtendedPrintDialog = true;
            this.zedGraphControl.ZoomEvent += new ZedGraph.ZedGraphControl.ZoomEventHandler(this.zedGraphControl_ZoomEvent);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox9, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 432F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1158, 432);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 72.76995F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(573, 426);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel9);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(567, 420);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Training Session Info";
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel10, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.textBoxNote, 0, 1);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 2;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 230F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(561, 401);
            this.tableLayoutPanel9.TabIndex = 9;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.Controls.Add(this.numericFTP, 1, 6);
            this.tableLayoutPanel10.Controls.Add(this.label19, 0, 6);
            this.tableLayoutPanel10.Controls.Add(this.textBoxDate, 1, 0);
            this.tableLayoutPanel10.Controls.Add(this.label5, 0, 7);
            this.tableLayoutPanel10.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.comboBoxSport, 1, 3);
            this.tableLayoutPanel10.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel10.Controls.Add(this.textBoxName, 1, 2);
            this.tableLayoutPanel10.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.textBoxTime, 1, 1);
            this.tableLayoutPanel10.Controls.Add(this.label16, 0, 4);
            this.tableLayoutPanel10.Controls.Add(this.comboBoxunit, 1, 4);
            this.tableLayoutPanel10.Controls.Add(this.label17, 0, 5);
            this.tableLayoutPanel10.Controls.Add(this.numericMaxHR, 1, 5);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 8;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(555, 224);
            this.tableLayoutPanel10.TabIndex = 0;
            // 
            // numericFTP
            // 
            this.numericFTP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numericFTP.Location = new System.Drawing.Point(280, 171);
            this.numericFTP.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numericFTP.Name = "numericFTP";
            this.numericFTP.Size = new System.Drawing.Size(272, 20);
            this.numericFTP.TabIndex = 14;
            this.numericFTP.ValueChanged += new System.EventHandler(this.numericFTP_ValueChanged);
            // 
            // label19
            // 
            this.label19.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(3, 175);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(156, 13);
            this.label19.TabIndex = 13;
            this.label19.Text = "Functional Threshold Power(w):";
            // 
            // textBoxDate
            // 
            this.textBoxDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxDate.Location = new System.Drawing.Point(280, 3);
            this.textBoxDate.Name = "textBoxDate";
            this.textBoxDate.Size = new System.Drawing.Size(272, 20);
            this.textBoxDate.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 211);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Notes:";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Date:";
            // 
            // comboBoxSport
            // 
            this.comboBoxSport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxSport.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSport.FormattingEnabled = true;
            this.comboBoxSport.Items.AddRange(new object[] {
            "Road bicycle",
            "Track cycling",
            "Cyclo-cross",
            "Mountain bike",
            "Cycle speedway"});
            this.comboBoxSport.Location = new System.Drawing.Point(280, 87);
            this.comboBoxSport.Name = "comboBoxSport";
            this.comboBoxSport.Size = new System.Drawing.Size(272, 21);
            this.comboBoxSport.TabIndex = 7;
            this.comboBoxSport.SelectedIndexChanged += new System.EventHandler(this.comboBoxSport_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Training name:";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 91);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Sport:";
            // 
            // textBoxName
            // 
            this.textBoxName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxName.Location = new System.Drawing.Point(280, 59);
            this.textBoxName.MaxLength = 30;
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(272, 20);
            this.textBoxName.TabIndex = 5;
            this.textBoxName.TextChanged += new System.EventHandler(this.textBoxName_TextChanged);
            this.textBoxName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxName_KeyDown);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Time:";
            // 
            // textBoxTime
            // 
            this.textBoxTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxTime.Location = new System.Drawing.Point(280, 31);
            this.textBoxTime.Name = "textBoxTime";
            this.textBoxTime.Size = new System.Drawing.Size(272, 20);
            this.textBoxTime.TabIndex = 3;
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(3, 119);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(34, 13);
            this.label16.TabIndex = 9;
            this.label16.Text = "Units:";
            // 
            // comboBoxunit
            // 
            this.comboBoxunit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxunit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxunit.FormattingEnabled = true;
            this.comboBoxunit.Items.AddRange(new object[] {
            "Imperial",
            "Metric"});
            this.comboBoxunit.Location = new System.Drawing.Point(280, 115);
            this.comboBoxunit.Name = "comboBoxunit";
            this.comboBoxunit.Size = new System.Drawing.Size(272, 21);
            this.comboBoxunit.TabIndex = 10;
            this.comboBoxunit.SelectedValueChanged += new System.EventHandler(this.comboBoxunit_SelectedValueChanged);
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(3, 147);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(76, 13);
            this.label17.TabIndex = 11;
            this.label17.Text = "Max HR(Bpm):";
            // 
            // numericMaxHR
            // 
            this.numericMaxHR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numericMaxHR.Location = new System.Drawing.Point(280, 143);
            this.numericMaxHR.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.numericMaxHR.Name = "numericMaxHR";
            this.numericMaxHR.Size = new System.Drawing.Size(272, 20);
            this.numericMaxHR.TabIndex = 12;
            this.numericMaxHR.ValueChanged += new System.EventHandler(this.numericMaxHR_ValueChanged);
            // 
            // textBoxNote
            // 
            this.textBoxNote.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxNote.Location = new System.Drawing.Point(3, 233);
            this.textBoxNote.Multiline = true;
            this.textBoxNote.Name = "textBoxNote";
            this.textBoxNote.Size = new System.Drawing.Size(555, 165);
            this.textBoxNote.TabIndex = 1;
            this.textBoxNote.TextChanged += new System.EventHandler(this.textBoxNote_TextChanged);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.tableLayoutPanel8);
            this.groupBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox9.Location = new System.Drawing.Point(582, 6);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(573, 423);
            this.groupBox9.TabIndex = 1;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Results";
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Controls.Add(this.label13, 0, 7);
            this.tableLayoutPanel8.Controls.Add(this.textBoxMaxCadence, 1, 2);
            this.tableLayoutPanel8.Controls.Add(this.label9, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this.textBoxCadenceAvg, 1, 1);
            this.tableLayoutPanel8.Controls.Add(this.textBoxModel, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.label8, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.label10, 0, 3);
            this.tableLayoutPanel8.Controls.Add(this.textBoxMaxSpeed, 1, 3);
            this.tableLayoutPanel8.Controls.Add(this.label11, 0, 5);
            this.tableLayoutPanel8.Controls.Add(this.label12, 0, 6);
            this.tableLayoutPanel8.Controls.Add(this.textBoxPwrAvg, 1, 5);
            this.tableLayoutPanel8.Controls.Add(this.textBoxPwrMax, 1, 6);
            this.tableLayoutPanel8.Controls.Add(this.label14, 0, 8);
            this.tableLayoutPanel8.Controls.Add(this.textBoxAvgPi, 1, 7);
            this.tableLayoutPanel8.Controls.Add(this.textBoxMaxPi, 1, 8);
            this.tableLayoutPanel8.Controls.Add(this.label15, 0, 9);
            this.tableLayoutPanel8.Controls.Add(this.textBoxPB, 1, 9);
            this.tableLayoutPanel8.Controls.Add(this.label18, 0, 10);
            this.tableLayoutPanel8.Controls.Add(this.textBoxHRPercentage, 1, 10);
            this.tableLayoutPanel8.Controls.Add(this.label20, 0, 11);
            this.tableLayoutPanel8.Controls.Add(this.textBoxFTP, 1, 11);
            this.tableLayoutPanel8.Controls.Add(this.label21, 0, 12);
            this.tableLayoutPanel8.Controls.Add(this.textBoxMaxAltitude, 1, 12);
            this.tableLayoutPanel8.Controls.Add(this.label22, 0, 4);
            this.tableLayoutPanel8.Controls.Add(this.textBoxNormPower, 1, 4);
            this.tableLayoutPanel8.Controls.Add(this.label23, 0, 13);
            this.tableLayoutPanel8.Controls.Add(this.textBoxIF, 1, 13);
            this.tableLayoutPanel8.Controls.Add(this.label24, 0, 14);
            this.tableLayoutPanel8.Controls.Add(this.textBoxTSS, 1, 14);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 16;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(567, 404);
            this.tableLayoutPanel8.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(3, 194);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(50, 16);
            this.label13.TabIndex = 12;
            this.label13.Text = "PI Avg:";
            // 
            // textBoxMaxCadence
            // 
            this.textBoxMaxCadence.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBoxMaxCadence.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMaxCadence.Location = new System.Drawing.Point(286, 57);
            this.textBoxMaxCadence.Name = "textBoxMaxCadence";
            this.textBoxMaxCadence.Size = new System.Drawing.Size(278, 22);
            this.textBoxMaxCadence.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(3, 59);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(94, 16);
            this.label9.TabIndex = 4;
            this.label9.Text = "Cadence Max:";
            // 
            // textBoxCadenceAvg
            // 
            this.textBoxCadenceAvg.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBoxCadenceAvg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCadenceAvg.Location = new System.Drawing.Point(286, 30);
            this.textBoxCadenceAvg.Name = "textBoxCadenceAvg";
            this.textBoxCadenceAvg.Size = new System.Drawing.Size(278, 22);
            this.textBoxCadenceAvg.TabIndex = 3;
            // 
            // textBoxModel
            // 
            this.textBoxModel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBoxModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxModel.Location = new System.Drawing.Point(286, 3);
            this.textBoxModel.Name = "textBoxModel";
            this.textBoxModel.Size = new System.Drawing.Size(278, 22);
            this.textBoxModel.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 16);
            this.label7.TabIndex = 0;
            this.label7.Text = "Model:";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 16);
            this.label8.TabIndex = 2;
            this.label8.Text = "Cadence Avg:";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(3, 86);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 16);
            this.label10.TabIndex = 6;
            this.label10.Text = "Speed Max:";
            // 
            // textBoxMaxSpeed
            // 
            this.textBoxMaxSpeed.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBoxMaxSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMaxSpeed.Location = new System.Drawing.Point(286, 84);
            this.textBoxMaxSpeed.Name = "textBoxMaxSpeed";
            this.textBoxMaxSpeed.Size = new System.Drawing.Size(278, 22);
            this.textBoxMaxSpeed.TabIndex = 7;
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(3, 140);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 16);
            this.label11.TabIndex = 8;
            this.label11.Text = "Power Avg:";
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(3, 167);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(77, 16);
            this.label12.TabIndex = 9;
            this.label12.Text = "Power Max:";
            // 
            // textBoxPwrAvg
            // 
            this.textBoxPwrAvg.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBoxPwrAvg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPwrAvg.Location = new System.Drawing.Point(286, 138);
            this.textBoxPwrAvg.Name = "textBoxPwrAvg";
            this.textBoxPwrAvg.Size = new System.Drawing.Size(278, 22);
            this.textBoxPwrAvg.TabIndex = 10;
            // 
            // textBoxPwrMax
            // 
            this.textBoxPwrMax.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxPwrMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPwrMax.Location = new System.Drawing.Point(286, 165);
            this.textBoxPwrMax.Name = "textBoxPwrMax";
            this.textBoxPwrMax.Size = new System.Drawing.Size(278, 22);
            this.textBoxPwrMax.TabIndex = 11;
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(3, 221);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(51, 16);
            this.label14.TabIndex = 13;
            this.label14.Text = "PI Max:";
            // 
            // textBoxAvgPi
            // 
            this.textBoxAvgPi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxAvgPi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAvgPi.Location = new System.Drawing.Point(286, 192);
            this.textBoxAvgPi.Name = "textBoxAvgPi";
            this.textBoxAvgPi.Size = new System.Drawing.Size(278, 22);
            this.textBoxAvgPi.TabIndex = 14;
            // 
            // textBoxMaxPi
            // 
            this.textBoxMaxPi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxMaxPi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMaxPi.Location = new System.Drawing.Point(286, 219);
            this.textBoxMaxPi.Name = "textBoxMaxPi";
            this.textBoxMaxPi.Size = new System.Drawing.Size(278, 22);
            this.textBoxMaxPi.TabIndex = 15;
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(3, 248);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(131, 16);
            this.label15.TabIndex = 16;
            this.label15.Text = "Power Balance(L/R):";
            // 
            // textBoxPB
            // 
            this.textBoxPB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxPB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPB.Location = new System.Drawing.Point(286, 246);
            this.textBoxPB.Name = "textBoxPB";
            this.textBoxPB.Size = new System.Drawing.Size(278, 22);
            this.textBoxPB.TabIndex = 18;
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(3, 275);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(132, 16);
            this.label18.TabIndex = 19;
            this.label18.Text = "Max HR Percentage:";
            // 
            // textBoxHRPercentage
            // 
            this.textBoxHRPercentage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxHRPercentage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxHRPercentage.Location = new System.Drawing.Point(286, 273);
            this.textBoxHRPercentage.Name = "textBoxHRPercentage";
            this.textBoxHRPercentage.Size = new System.Drawing.Size(278, 22);
            this.textBoxHRPercentage.TabIndex = 21;
            // 
            // label20
            // 
            this.label20.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(3, 302);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(110, 16);
            this.label20.TabIndex = 22;
            this.label20.Text = "FTP Percentage:";
            // 
            // textBoxFTP
            // 
            this.textBoxFTP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxFTP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxFTP.Location = new System.Drawing.Point(286, 300);
            this.textBoxFTP.Name = "textBoxFTP";
            this.textBoxFTP.Size = new System.Drawing.Size(278, 22);
            this.textBoxFTP.TabIndex = 23;
            // 
            // label21
            // 
            this.label21.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(3, 329);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(83, 16);
            this.label21.TabIndex = 24;
            this.label21.Text = "Max Altitude:";
            // 
            // textBoxMaxAltitude
            // 
            this.textBoxMaxAltitude.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxMaxAltitude.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMaxAltitude.Location = new System.Drawing.Point(286, 327);
            this.textBoxMaxAltitude.Name = "textBoxMaxAltitude";
            this.textBoxMaxAltitude.Size = new System.Drawing.Size(278, 22);
            this.textBoxMaxAltitude.TabIndex = 25;
            // 
            // label22
            // 
            this.label22.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(3, 113);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(122, 16);
            this.label22.TabIndex = 26;
            this.label22.Text = "Normalised Power:";
            // 
            // textBoxNormPower
            // 
            this.textBoxNormPower.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxNormPower.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNormPower.Location = new System.Drawing.Point(286, 111);
            this.textBoxNormPower.Name = "textBoxNormPower";
            this.textBoxNormPower.Size = new System.Drawing.Size(278, 22);
            this.textBoxNormPower.TabIndex = 27;
            // 
            // label23
            // 
            this.label23.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(3, 356);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(100, 16);
            this.label23.TabIndex = 28;
            this.label23.Text = "Intensity Factor:";
            // 
            // textBoxIF
            // 
            this.textBoxIF.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxIF.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxIF.Location = new System.Drawing.Point(286, 354);
            this.textBoxIF.Name = "textBoxIF";
            this.textBoxIF.Size = new System.Drawing.Size(278, 22);
            this.textBoxIF.TabIndex = 29;
            // 
            // label24
            // 
            this.label24.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(3, 383);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(38, 16);
            this.label24.TabIndex = 30;
            this.label24.Text = "TSS:";
            // 
            // textBoxTSS
            // 
            this.textBoxTSS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxTSS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTSS.Location = new System.Drawing.Point(286, 381);
            this.textBoxTSS.Name = "textBoxTSS";
            this.textBoxTSS.Size = new System.Drawing.Size(278, 22);
            this.textBoxTSS.TabIndex = 31;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dataGridView1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1170, 613);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Data";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1164, 607);
            this.dataGridView1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tableLayoutPanel13);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1170, 613);
            this.tabPage2.TabIndex = 3;
            this.tabPage2.Text = "Comparison";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 2;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel13.Controls.Add(this.groupBox13, 1, 2);
            this.tableLayoutPanel13.Controls.Add(this.comboBoxComparison, 1, 1);
            this.tableLayoutPanel13.Controls.Add(this.groupBox10, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.groupBox11, 0, 2);
            this.tableLayoutPanel13.Controls.Add(this.groupBox12, 1, 0);
            this.tableLayoutPanel13.Controls.Add(this.tableLayoutPanel16, 0, 1);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 3;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(1164, 607);
            this.tableLayoutPanel13.TabIndex = 0;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.tableLayoutPanel15);
            this.groupBox13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox13.Location = new System.Drawing.Point(934, 326);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(227, 278);
            this.groupBox13.TabIndex = 37;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Selected Session Info:";
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 2;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.66666F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel15.Controls.Add(this.label35, 0, 0);
            this.tableLayoutPanel15.Controls.Add(this.label36, 0, 1);
            this.tableLayoutPanel15.Controls.Add(this.label37, 0, 2);
            this.tableLayoutPanel15.Controls.Add(this.label38, 0, 3);
            this.tableLayoutPanel15.Controls.Add(this.label39, 0, 4);
            this.tableLayoutPanel15.Controls.Add(this.label40, 0, 5);
            this.tableLayoutPanel15.Controls.Add(this.label41, 0, 6);
            this.tableLayoutPanel15.Controls.Add(this.label42, 0, 7);
            this.tableLayoutPanel15.Controls.Add(this.label43, 0, 8);
            this.tableLayoutPanel15.Controls.Add(this.textBoxSsCadenceAverage, 1, 0);
            this.tableLayoutPanel15.Controls.Add(this.textBoxSsCadenceMax, 1, 1);
            this.tableLayoutPanel15.Controls.Add(this.textBoxSsSpeedAverage, 1, 2);
            this.tableLayoutPanel15.Controls.Add(this.textBoxSsSpeedMax, 1, 3);
            this.tableLayoutPanel15.Controls.Add(this.textBoxSsNormalisedPower, 1, 4);
            this.tableLayoutPanel15.Controls.Add(this.textBoxSsPowerAverage, 1, 5);
            this.tableLayoutPanel15.Controls.Add(this.textBoxSsPowerMax, 1, 6);
            this.tableLayoutPanel15.Controls.Add(this.textBoxSsHRAverage, 1, 7);
            this.tableLayoutPanel15.Controls.Add(this.textBoxSsHRMax, 1, 8);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 10;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(221, 259);
            this.tableLayoutPanel15.TabIndex = 1;
            // 
            // label35
            // 
            this.label35.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(3, 5);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(121, 16);
            this.label35.TabIndex = 1;
            this.label35.Text = "Cadence Average:";
            // 
            // label36
            // 
            this.label36.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(3, 32);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(94, 16);
            this.label36.TabIndex = 2;
            this.label36.Text = "Cadence Max:";
            // 
            // label37
            // 
            this.label37.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(3, 59);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(83, 16);
            this.label37.TabIndex = 3;
            this.label37.Text = "Speed AVG:";
            // 
            // label38
            // 
            this.label38.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(3, 86);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(80, 16);
            this.label38.TabIndex = 4;
            this.label38.Text = "Speed Max:";
            // 
            // label39
            // 
            this.label39.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(3, 113);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(122, 16);
            this.label39.TabIndex = 5;
            this.label39.Text = "Normalised Power:";
            // 
            // label40
            // 
            this.label40.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(3, 140);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(83, 16);
            this.label40.TabIndex = 6;
            this.label40.Text = "Power AVG::";
            // 
            // label41
            // 
            this.label41.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(3, 167);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(80, 16);
            this.label41.TabIndex = 7;
            this.label41.Text = "Power Max::";
            // 
            // label42
            // 
            this.label42.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(3, 194);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(65, 16);
            this.label42.TabIndex = 8;
            this.label42.Text = "HR AVG::";
            // 
            // label43
            // 
            this.label43.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(3, 221);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(62, 16);
            this.label43.TabIndex = 9;
            this.label43.Text = "HR Max::";
            // 
            // textBoxSsCadenceAverage
            // 
            this.textBoxSsCadenceAverage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSsCadenceAverage.Location = new System.Drawing.Point(150, 3);
            this.textBoxSsCadenceAverage.Name = "textBoxSsCadenceAverage";
            this.textBoxSsCadenceAverage.Size = new System.Drawing.Size(68, 22);
            this.textBoxSsCadenceAverage.TabIndex = 10;
            // 
            // textBoxSsCadenceMax
            // 
            this.textBoxSsCadenceMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSsCadenceMax.Location = new System.Drawing.Point(150, 30);
            this.textBoxSsCadenceMax.Name = "textBoxSsCadenceMax";
            this.textBoxSsCadenceMax.Size = new System.Drawing.Size(68, 22);
            this.textBoxSsCadenceMax.TabIndex = 11;
            // 
            // textBoxSsSpeedAverage
            // 
            this.textBoxSsSpeedAverage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSsSpeedAverage.Location = new System.Drawing.Point(150, 57);
            this.textBoxSsSpeedAverage.Name = "textBoxSsSpeedAverage";
            this.textBoxSsSpeedAverage.Size = new System.Drawing.Size(68, 22);
            this.textBoxSsSpeedAverage.TabIndex = 12;
            // 
            // textBoxSsSpeedMax
            // 
            this.textBoxSsSpeedMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSsSpeedMax.Location = new System.Drawing.Point(150, 84);
            this.textBoxSsSpeedMax.Name = "textBoxSsSpeedMax";
            this.textBoxSsSpeedMax.Size = new System.Drawing.Size(68, 22);
            this.textBoxSsSpeedMax.TabIndex = 13;
            // 
            // textBoxSsNormalisedPower
            // 
            this.textBoxSsNormalisedPower.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSsNormalisedPower.Location = new System.Drawing.Point(150, 111);
            this.textBoxSsNormalisedPower.Name = "textBoxSsNormalisedPower";
            this.textBoxSsNormalisedPower.Size = new System.Drawing.Size(68, 22);
            this.textBoxSsNormalisedPower.TabIndex = 14;
            // 
            // textBoxSsPowerAverage
            // 
            this.textBoxSsPowerAverage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSsPowerAverage.Location = new System.Drawing.Point(150, 138);
            this.textBoxSsPowerAverage.Name = "textBoxSsPowerAverage";
            this.textBoxSsPowerAverage.Size = new System.Drawing.Size(68, 22);
            this.textBoxSsPowerAverage.TabIndex = 15;
            // 
            // textBoxSsPowerMax
            // 
            this.textBoxSsPowerMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSsPowerMax.Location = new System.Drawing.Point(150, 165);
            this.textBoxSsPowerMax.Name = "textBoxSsPowerMax";
            this.textBoxSsPowerMax.Size = new System.Drawing.Size(68, 22);
            this.textBoxSsPowerMax.TabIndex = 16;
            // 
            // textBoxSsHRAverage
            // 
            this.textBoxSsHRAverage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSsHRAverage.Location = new System.Drawing.Point(150, 192);
            this.textBoxSsHRAverage.Name = "textBoxSsHRAverage";
            this.textBoxSsHRAverage.Size = new System.Drawing.Size(68, 22);
            this.textBoxSsHRAverage.TabIndex = 17;
            // 
            // textBoxSsHRMax
            // 
            this.textBoxSsHRMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSsHRMax.Location = new System.Drawing.Point(150, 219);
            this.textBoxSsHRMax.Name = "textBoxSsHRMax";
            this.textBoxSsHRMax.Size = new System.Drawing.Size(68, 22);
            this.textBoxSsHRMax.TabIndex = 18;
            // 
            // comboBoxComparison
            // 
            this.comboBoxComparison.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comboBoxComparison.BackColor = System.Drawing.SystemColors.Window;
            this.comboBoxComparison.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxComparison.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxComparison.FormattingEnabled = true;
            this.comboBoxComparison.Location = new System.Drawing.Point(934, 291);
            this.comboBoxComparison.Name = "comboBoxComparison";
            this.comboBoxComparison.Size = new System.Drawing.Size(227, 24);
            this.comboBoxComparison.TabIndex = 32;
            this.comboBoxComparison.SelectedIndexChanged += new System.EventHandler(this.comboBoxComparison_SelectedIndexChanged);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.ComparisonGraphControl1);
            this.groupBox10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox10.Location = new System.Drawing.Point(3, 3);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(925, 277);
            this.groupBox10.TabIndex = 34;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Current Session:";
            // 
            // ComparisonGraphControl1
            // 
            this.ComparisonGraphControl1.AutoScroll = true;
            this.ComparisonGraphControl1.Cursor = System.Windows.Forms.Cursors.VSplit;
            this.ComparisonGraphControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ComparisonGraphControl1.IsAntiAlias = true;
            this.ComparisonGraphControl1.IsEnableHPan = false;
            this.ComparisonGraphControl1.IsEnableHZoom = false;
            this.ComparisonGraphControl1.IsEnableVPan = false;
            this.ComparisonGraphControl1.IsEnableVZoom = false;
            this.ComparisonGraphControl1.IsEnableWheelZoom = false;
            this.ComparisonGraphControl1.Location = new System.Drawing.Point(3, 16);
            this.ComparisonGraphControl1.Margin = new System.Windows.Forms.Padding(0);
            this.ComparisonGraphControl1.Name = "ComparisonGraphControl1";
            this.ComparisonGraphControl1.ScrollGrace = 0D;
            this.ComparisonGraphControl1.ScrollMaxX = 0D;
            this.ComparisonGraphControl1.ScrollMaxY = 0D;
            this.ComparisonGraphControl1.ScrollMaxY2 = 0D;
            this.ComparisonGraphControl1.ScrollMinX = 0D;
            this.ComparisonGraphControl1.ScrollMinY = 0D;
            this.ComparisonGraphControl1.ScrollMinY2 = 0D;
            this.ComparisonGraphControl1.Size = new System.Drawing.Size(919, 258);
            this.ComparisonGraphControl1.TabIndex = 2;
            this.ComparisonGraphControl1.UseExtendedPrintDialog = true;
            this.ComparisonGraphControl1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ComparisonGraphControl1_MouseMove);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.ComparisonGraphControl2);
            this.groupBox11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox11.Location = new System.Drawing.Point(3, 326);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(925, 278);
            this.groupBox11.TabIndex = 35;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Comparison Session:";
            // 
            // ComparisonGraphControl2
            // 
            this.ComparisonGraphControl2.AutoScroll = true;
            this.ComparisonGraphControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ComparisonGraphControl2.IsAntiAlias = true;
            this.ComparisonGraphControl2.IsEnableWheelZoom = false;
            this.ComparisonGraphControl2.Location = new System.Drawing.Point(3, 16);
            this.ComparisonGraphControl2.Margin = new System.Windows.Forms.Padding(0);
            this.ComparisonGraphControl2.Name = "ComparisonGraphControl2";
            this.ComparisonGraphControl2.ScrollGrace = 0D;
            this.ComparisonGraphControl2.ScrollMaxX = 0D;
            this.ComparisonGraphControl2.ScrollMaxY = 0D;
            this.ComparisonGraphControl2.ScrollMaxY2 = 0D;
            this.ComparisonGraphControl2.ScrollMinX = 0D;
            this.ComparisonGraphControl2.ScrollMinY = 0D;
            this.ComparisonGraphControl2.ScrollMinY2 = 0D;
            this.ComparisonGraphControl2.Size = new System.Drawing.Size(919, 259);
            this.ComparisonGraphControl2.TabIndex = 34;
            this.ComparisonGraphControl2.UseExtendedPrintDialog = true;
            this.ComparisonGraphControl2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ComparisonGraphControl2_MouseMove);
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.tableLayoutPanel14);
            this.groupBox12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox12.Location = new System.Drawing.Point(934, 3);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(227, 277);
            this.groupBox12.TabIndex = 36;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Current Session Info:";
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 2;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.66666F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel14.Controls.Add(this.label26, 0, 0);
            this.tableLayoutPanel14.Controls.Add(this.label27, 0, 1);
            this.tableLayoutPanel14.Controls.Add(this.label28, 0, 2);
            this.tableLayoutPanel14.Controls.Add(this.label29, 0, 3);
            this.tableLayoutPanel14.Controls.Add(this.label30, 0, 4);
            this.tableLayoutPanel14.Controls.Add(this.label31, 0, 5);
            this.tableLayoutPanel14.Controls.Add(this.label32, 0, 6);
            this.tableLayoutPanel14.Controls.Add(this.label33, 0, 7);
            this.tableLayoutPanel14.Controls.Add(this.label34, 0, 8);
            this.tableLayoutPanel14.Controls.Add(this.textBoxCsCadenceAverage, 1, 0);
            this.tableLayoutPanel14.Controls.Add(this.textBoxCsCadenceMax, 1, 1);
            this.tableLayoutPanel14.Controls.Add(this.textBoxCsSpeedAverage, 1, 2);
            this.tableLayoutPanel14.Controls.Add(this.textBoxCsSpeedMax, 1, 3);
            this.tableLayoutPanel14.Controls.Add(this.textBoxCsNormalisedPower, 1, 4);
            this.tableLayoutPanel14.Controls.Add(this.textBoxCsPowerAverage, 1, 5);
            this.tableLayoutPanel14.Controls.Add(this.textBoxCsPowerMax, 1, 6);
            this.tableLayoutPanel14.Controls.Add(this.textBoxCsHRAverage, 1, 7);
            this.tableLayoutPanel14.Controls.Add(this.textBoxCsHRMax, 1, 8);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 10;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(221, 258);
            this.tableLayoutPanel14.TabIndex = 0;
            // 
            // label26
            // 
            this.label26.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(3, 5);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(121, 16);
            this.label26.TabIndex = 1;
            this.label26.Text = "Cadence Average:";
            // 
            // label27
            // 
            this.label27.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(3, 32);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(94, 16);
            this.label27.TabIndex = 2;
            this.label27.Text = "Cadence Max:";
            // 
            // label28
            // 
            this.label28.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(3, 59);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(83, 16);
            this.label28.TabIndex = 3;
            this.label28.Text = "Speed AVG:";
            // 
            // label29
            // 
            this.label29.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(3, 86);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(80, 16);
            this.label29.TabIndex = 4;
            this.label29.Text = "Speed Max:";
            // 
            // label30
            // 
            this.label30.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(3, 113);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(122, 16);
            this.label30.TabIndex = 5;
            this.label30.Text = "Normalised Power:";
            // 
            // label31
            // 
            this.label31.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(3, 140);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(83, 16);
            this.label31.TabIndex = 6;
            this.label31.Text = "Power AVG::";
            // 
            // label32
            // 
            this.label32.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(3, 167);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(80, 16);
            this.label32.TabIndex = 7;
            this.label32.Text = "Power Max::";
            // 
            // label33
            // 
            this.label33.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(3, 194);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(65, 16);
            this.label33.TabIndex = 8;
            this.label33.Text = "HR AVG::";
            // 
            // label34
            // 
            this.label34.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(3, 221);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(62, 16);
            this.label34.TabIndex = 9;
            this.label34.Text = "HR Max::";
            // 
            // textBoxCsCadenceAverage
            // 
            this.textBoxCsCadenceAverage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCsCadenceAverage.Location = new System.Drawing.Point(150, 3);
            this.textBoxCsCadenceAverage.Name = "textBoxCsCadenceAverage";
            this.textBoxCsCadenceAverage.Size = new System.Drawing.Size(68, 22);
            this.textBoxCsCadenceAverage.TabIndex = 10;
            // 
            // textBoxCsCadenceMax
            // 
            this.textBoxCsCadenceMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCsCadenceMax.Location = new System.Drawing.Point(150, 30);
            this.textBoxCsCadenceMax.Name = "textBoxCsCadenceMax";
            this.textBoxCsCadenceMax.Size = new System.Drawing.Size(68, 22);
            this.textBoxCsCadenceMax.TabIndex = 11;
            // 
            // textBoxCsSpeedAverage
            // 
            this.textBoxCsSpeedAverage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCsSpeedAverage.Location = new System.Drawing.Point(150, 57);
            this.textBoxCsSpeedAverage.Name = "textBoxCsSpeedAverage";
            this.textBoxCsSpeedAverage.Size = new System.Drawing.Size(68, 22);
            this.textBoxCsSpeedAverage.TabIndex = 12;
            // 
            // textBoxCsSpeedMax
            // 
            this.textBoxCsSpeedMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCsSpeedMax.Location = new System.Drawing.Point(150, 84);
            this.textBoxCsSpeedMax.Name = "textBoxCsSpeedMax";
            this.textBoxCsSpeedMax.Size = new System.Drawing.Size(68, 22);
            this.textBoxCsSpeedMax.TabIndex = 13;
            // 
            // textBoxCsNormalisedPower
            // 
            this.textBoxCsNormalisedPower.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCsNormalisedPower.Location = new System.Drawing.Point(150, 111);
            this.textBoxCsNormalisedPower.Name = "textBoxCsNormalisedPower";
            this.textBoxCsNormalisedPower.Size = new System.Drawing.Size(68, 22);
            this.textBoxCsNormalisedPower.TabIndex = 14;
            // 
            // textBoxCsPowerAverage
            // 
            this.textBoxCsPowerAverage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCsPowerAverage.Location = new System.Drawing.Point(150, 138);
            this.textBoxCsPowerAverage.Name = "textBoxCsPowerAverage";
            this.textBoxCsPowerAverage.Size = new System.Drawing.Size(68, 22);
            this.textBoxCsPowerAverage.TabIndex = 15;
            // 
            // textBoxCsPowerMax
            // 
            this.textBoxCsPowerMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCsPowerMax.Location = new System.Drawing.Point(150, 165);
            this.textBoxCsPowerMax.Name = "textBoxCsPowerMax";
            this.textBoxCsPowerMax.Size = new System.Drawing.Size(68, 22);
            this.textBoxCsPowerMax.TabIndex = 16;
            // 
            // textBoxCsHRAverage
            // 
            this.textBoxCsHRAverage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCsHRAverage.Location = new System.Drawing.Point(150, 192);
            this.textBoxCsHRAverage.Name = "textBoxCsHRAverage";
            this.textBoxCsHRAverage.Size = new System.Drawing.Size(68, 22);
            this.textBoxCsHRAverage.TabIndex = 17;
            // 
            // textBoxCsHRMax
            // 
            this.textBoxCsHRMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCsHRMax.Location = new System.Drawing.Point(150, 219);
            this.textBoxCsHRMax.Name = "textBoxCsHRMax";
            this.textBoxCsHRMax.Size = new System.Drawing.Size(68, 22);
            this.textBoxCsHRMax.TabIndex = 18;
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 2;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 76.75676F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.24324F));
            this.tableLayoutPanel16.Controls.Add(this.label25, 1, 0);
            this.tableLayoutPanel16.Controls.Add(this.tableLayoutPanel17, 0, 0);
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(3, 286);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 1;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(925, 34);
            this.tableLayoutPanel16.TabIndex = 38;
            // 
            // label25
            // 
            this.label25.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(731, 10);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(191, 13);
            this.label25.TabIndex = 3;
            this.label25.Text = "Select a session for comparison:";
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.ColumnCount = 5;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel17.Controls.Add(this.textBoxCadenceComp, 0, 0);
            this.tableLayoutPanel17.Controls.Add(this.textBoxAltitudeComp, 0, 0);
            this.tableLayoutPanel17.Controls.Add(this.textBoxHRComp, 0, 0);
            this.tableLayoutPanel17.Controls.Add(this.textBoxPowerComp, 1, 0);
            this.tableLayoutPanel17.Controls.Add(this.textBoxSpeedComp, 0, 0);
            this.tableLayoutPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel17.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 1;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(704, 28);
            this.tableLayoutPanel17.TabIndex = 4;
            // 
            // textBoxCadenceComp
            // 
            this.textBoxCadenceComp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxCadenceComp.Location = new System.Drawing.Point(283, 3);
            this.textBoxCadenceComp.Name = "textBoxCadenceComp";
            this.textBoxCadenceComp.Size = new System.Drawing.Size(134, 20);
            this.textBoxCadenceComp.TabIndex = 4;
            // 
            // textBoxAltitudeComp
            // 
            this.textBoxAltitudeComp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxAltitudeComp.Location = new System.Drawing.Point(423, 3);
            this.textBoxAltitudeComp.Name = "textBoxAltitudeComp";
            this.textBoxAltitudeComp.Size = new System.Drawing.Size(134, 20);
            this.textBoxAltitudeComp.TabIndex = 3;
            // 
            // textBoxHRComp
            // 
            this.textBoxHRComp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxHRComp.Location = new System.Drawing.Point(3, 3);
            this.textBoxHRComp.Name = "textBoxHRComp";
            this.textBoxHRComp.Size = new System.Drawing.Size(134, 20);
            this.textBoxHRComp.TabIndex = 2;
            // 
            // textBoxPowerComp
            // 
            this.textBoxPowerComp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxPowerComp.Location = new System.Drawing.Point(563, 3);
            this.textBoxPowerComp.Name = "textBoxPowerComp";
            this.textBoxPowerComp.Size = new System.Drawing.Size(138, 20);
            this.textBoxPowerComp.TabIndex = 1;
            // 
            // textBoxSpeedComp
            // 
            this.textBoxSpeedComp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxSpeedComp.Location = new System.Drawing.Point(143, 3);
            this.textBoxSpeedComp.Name = "textBoxSpeedComp";
            this.textBoxSpeedComp.Size = new System.Drawing.Size(134, 20);
            this.textBoxSpeedComp.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.Controls.Add(this.groupBox8, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.groupBox6, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.groupBox5, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.groupBox7, 3, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(592, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(583, 68);
            this.tableLayoutPanel4.TabIndex = 2;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.labelSpeed);
            this.groupBox8.Controls.Add(this.label6);
            this.groupBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox8.Location = new System.Drawing.Point(293, 3);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(139, 62);
            this.groupBox8.TabIndex = 10;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Avg Speed";
            // 
            // labelSpeed
            // 
            this.labelSpeed.AutoSize = true;
            this.labelSpeed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSpeed.Location = new System.Drawing.Point(3, 16);
            this.labelSpeed.Name = "labelSpeed";
            this.labelSpeed.Size = new System.Drawing.Size(0, 18);
            this.labelSpeed.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 18);
            this.label6.TabIndex = 1;
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.labelDistance);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox6.Location = new System.Drawing.Point(148, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(139, 62);
            this.groupBox6.TabIndex = 8;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Distance";
            // 
            // labelDistance
            // 
            this.labelDistance.AutoSize = true;
            this.labelDistance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDistance.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDistance.Location = new System.Drawing.Point(3, 16);
            this.labelDistance.Name = "labelDistance";
            this.labelDistance.Size = new System.Drawing.Size(0, 18);
            this.labelDistance.TabIndex = 1;
            this.labelDistance.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.labelCalories);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(3, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(139, 62);
            this.groupBox5.TabIndex = 7;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Calories";
            // 
            // labelCalories
            // 
            this.labelCalories.AutoSize = true;
            this.labelCalories.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCalories.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCalories.Location = new System.Drawing.Point(3, 16);
            this.labelCalories.Name = "labelCalories";
            this.labelCalories.Size = new System.Drawing.Size(0, 18);
            this.labelCalories.TabIndex = 1;
            this.labelCalories.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.labelAltitude);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox7.Location = new System.Drawing.Point(438, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(142, 62);
            this.groupBox7.TabIndex = 11;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Altitude:";
            // 
            // labelAltitude
            // 
            this.labelAltitude.AutoSize = true;
            this.labelAltitude.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelAltitude.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAltitude.Location = new System.Drawing.Point(3, 16);
            this.labelAltitude.Name = "labelAltitude";
            this.labelAltitude.Size = new System.Drawing.Size(0, 18);
            this.labelAltitude.TabIndex = 0;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel6, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.tabControl, 0, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 24);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1184, 725);
            this.tableLayoutPanel5.TabIndex = 3;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel4, 1, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(1178, 74);
            this.tableLayoutPanel6.TabIndex = 0;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 220F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.Controls.Add(this.groupBox4, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.groupBox2, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.groupBox3, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel7, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(583, 68);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.labelHRMax);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(465, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(115, 62);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "HR Max";
            // 
            // labelHRMax
            // 
            this.labelHRMax.AutoSize = true;
            this.labelHRMax.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelHRMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHRMax.Location = new System.Drawing.Point(3, 16);
            this.labelHRMax.Name = "labelHRMax";
            this.labelHRMax.Size = new System.Drawing.Size(0, 18);
            this.labelHRMax.TabIndex = 0;
            this.labelHRMax.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.labelDuration);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(223, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(115, 62);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Duration:";
            // 
            // labelDuration
            // 
            this.labelDuration.AutoSize = true;
            this.labelDuration.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDuration.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDuration.Location = new System.Drawing.Point(3, 16);
            this.labelDuration.Name = "labelDuration";
            this.labelDuration.Size = new System.Drawing.Size(0, 18);
            this.labelDuration.TabIndex = 0;
            this.labelDuration.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.labelHRAvg);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(344, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(115, 62);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "HR Avg";
            // 
            // labelHRAvg
            // 
            this.labelHRAvg.AutoSize = true;
            this.labelHRAvg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelHRAvg.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHRAvg.Location = new System.Drawing.Point(3, 16);
            this.labelHRAvg.Name = "labelHRAvg";
            this.labelHRAvg.Size = new System.Drawing.Size(0, 18);
            this.labelHRAvg.TabIndex = 0;
            this.labelHRAvg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel12, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(214, 62);
            this.tableLayoutPanel7.TabIndex = 7;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 1;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.Controls.Add(this.labelDate, 0, 1);
            this.tableLayoutPanel12.Controls.Add(this.comboBoxSession, 0, 0);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 2;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(208, 56);
            this.tableLayoutPanel12.TabIndex = 4;
            // 
            // labelDate
            // 
            this.labelDate.AutoSize = true;
            this.labelDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDate.Location = new System.Drawing.Point(3, 28);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(202, 28);
            this.labelDate.TabIndex = 32;
            // 
            // comboBoxSession
            // 
            this.comboBoxSession.BackColor = System.Drawing.SystemColors.Window;
            this.comboBoxSession.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxSession.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSession.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxSession.FormattingEnabled = true;
            this.comboBoxSession.Location = new System.Drawing.Point(3, 3);
            this.comboBoxSession.Name = "comboBoxSession";
            this.comboBoxSession.Size = new System.Drawing.Size(202, 24);
            this.comboBoxSession.TabIndex = 31;
            this.comboBoxSession.SelectedIndexChanged += new System.EventHandler(this.comboBoxSession_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1184, 749);
            this.Controls.Add(this.tableLayoutPanel5);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.Text = "BykerData";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericFTP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMaxHR)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel15.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel14.PerformLayout();
            this.tableLayoutPanel16.ResumeLayout(false);
            this.tableLayoutPanel16.PerformLayout();
            this.tableLayoutPanel17.ResumeLayout(false);
            this.tableLayoutPanel17.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label labelDistance;
        private System.Windows.Forms.Label labelCalories;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
        private GroupBox groupBox8;
        private Label label6;
        private GroupBox groupBox7;
        private Label labelAltitude;
        private Label labelSpeed;
        private TableLayoutPanel tableLayoutPanel3;
        private GroupBox groupBox4;
        private Label labelHRMax;
        private GroupBox groupBox2;
        private Label labelDuration;
        private GroupBox groupBox3;
        private Label labelHRAvg;
        private TableLayoutPanel tableLayoutPanel7;
        private TabPage tabPage3;
        private DataGridView dataGridView1;
        private TableLayoutPanel tableLayoutPanel11;
        private ZedGraph.ZedGraphControl zedGraphControl;
        private TableLayoutPanel tableLayoutPanel1;
        private TableLayoutPanel tableLayoutPanel2;
        private GroupBox groupBox1;
        private TableLayoutPanel tableLayoutPanel9;
        private TableLayoutPanel tableLayoutPanel10;
        private TextBox textBoxDate;
        private Label label5;
        private Label label1;
        private ComboBox comboBoxSport;
        private Label label3;
        private Label label4;
        private TextBox textBoxName;
        private Label label2;
        private TextBox textBoxTime;
        private TextBox textBoxNote;
        private GroupBox groupBox9;
        private TableLayoutPanel tableLayoutPanel8;
        private Label label13;
        private TextBox textBoxMaxCadence;
        private Label label9;
        private TextBox textBoxCadenceAvg;
        private TextBox textBoxModel;
        private Label label7;
        private Label label8;
        private Label label10;
        private TextBox textBoxMaxSpeed;
        private Label label11;
        private Label label12;
        private TextBox textBoxPwrAvg;
        private TextBox textBoxPwrMax;
        private Label label14;
        private TextBox textBoxAvgPi;
        private TextBox textBoxMaxPi;
        private Label label15;
        private TextBox textBoxPB;
        private Label label16;
        private ComboBox comboBoxunit;
        private Label label17;
        private NumericUpDown numericMaxHR;
        private Label label18;
        private TextBox textBoxHRPercentage;
        private NumericUpDown numericFTP;
        private Label label19;
        private Label label20;
        private TextBox textBoxFTP;
        private Label label21;
        private TextBox textBoxMaxAltitude;
        private Label label22;
        private TextBox textBoxNormPower;
        private Label label23;
        private TextBox textBoxIF;
        private TableLayoutPanel tableLayoutPanel12;
        private ComboBox comboBoxSession;
        private Label labelDate;
        private Label label24;
        private TextBox textBoxTSS;
        private TabPage tabPage2;
        private TableLayoutPanel tableLayoutPanel13;
        private ComboBox comboBoxComparison;
        private GroupBox groupBox10;
        private ZedGraph.ZedGraphControl ComparisonGraphControl1;
        private GroupBox groupBox11;
        private ZedGraph.ZedGraphControl ComparisonGraphControl2;
        private GroupBox groupBox13;
        private GroupBox groupBox12;
        private TableLayoutPanel tableLayoutPanel14;
        private Label label26;
        private Label label27;
        private Label label28;
        private Label label29;
        private Label label30;
        private Label label31;
        private Label label32;
        private Label label33;
        private Label label34;
        private TextBox textBoxCsCadenceAverage;
        private TextBox textBoxCsCadenceMax;
        private TextBox textBoxCsSpeedAverage;
        private TextBox textBoxCsSpeedMax;
        private TextBox textBoxCsNormalisedPower;
        private TextBox textBoxCsPowerAverage;
        private TextBox textBoxCsPowerMax;
        private TextBox textBoxCsHRAverage;
        private TextBox textBoxCsHRMax;
        private TableLayoutPanel tableLayoutPanel15;
        private Label label35;
        private Label label36;
        private Label label37;
        private Label label38;
        private Label label39;
        private Label label40;
        private Label label41;
        private Label label42;
        private Label label43;
        private TextBox textBoxSsCadenceAverage;
        private TextBox textBoxSsCadenceMax;
        private TextBox textBoxSsSpeedAverage;
        private TextBox textBoxSsSpeedMax;
        private TextBox textBoxSsNormalisedPower;
        private TextBox textBoxSsPowerAverage;
        private TextBox textBoxSsPowerMax;
        private TextBox textBoxSsHRAverage;
        private TextBox textBoxSsHRMax;
        private TableLayoutPanel tableLayoutPanel16;
        private Label label25;
        private TableLayoutPanel tableLayoutPanel17;
        private TextBox textBoxPowerComp;
        private TextBox textBoxSpeedComp;
        private TextBox textBoxCadenceComp;
        private TextBox textBoxAltitudeComp;
        private TextBox textBoxHRComp;
    }
}

